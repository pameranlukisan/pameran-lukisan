<?php

class Login extends Controller
{
    // public function __construct()
    // {
    //     if (isset($_SESSION['login'])) {
    //         header('location: ' . BASEURL . '/home');
    //     }
    // }
    public function index()
    {
        $data['judul'] = "Login";
        // $this->view('templates/header', $data);
        $this->view('login/index', $data);
    }

    public function auth()
    {
        if ($this->model('User_model')->login($_POST)) {
            $user = $this->model('User_model')->login($_POST);
            $_SESSION['login'] = true;
            if($user["role"] == 1){
                header('location: ' . BASEURL . '/admin');
                die;
            }else{
                header('location: ' . BASEURL . '/user');
                die;
            }
        } else {
            echo '
            <script>
                window.location.href="'.BASEURL.'/login";
            </script>';
        }
    }

    public function logout()
    {
        session_destroy();
        header('Location: ' . BASEURL . '/login');
        exit;
    }
}
