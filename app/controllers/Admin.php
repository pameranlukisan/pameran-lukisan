<?php
class Admin extends Controller
{

    public function __construct()
    {
        if (!isset($_SESSION['login'])) {
            header('location: ' . BASEURL . '/login');
        }
    }
    public function index()
    {
        // echo "Home/index";
        $data["judul"] = "Admin";
        $data["nama"] = $this->model("User_model")->getUser();
        $this->view("templates/header", $data);
        $this->view("admin/index", $data);
        $this->view("templates/footer");
    }
}
