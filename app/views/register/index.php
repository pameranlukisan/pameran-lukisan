<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman <?= $data['judul']; ?></title>
    <link rel="stylesheet" href="<?= BASEURL; ?>/css/bootstrap.css">
</head>

<body class="bg-warning">
<div class="container my-5">
    <div class="d-flex justify-content-center m-2">
        <div class="col-xl-10 col-lg-12">
            <div class="card shadow-lg rounded">
                <div class="card-body-0">
                    <div class="row">
                        <div class="col-lg-6" style="background-image: url('<?= BASEURL; ?>/img/login.jpg'); background-size:100%; background-position: center;"></div>
                        <div class="col-lg-6 p-5">
                            <div class="m-5">
                                <div class="mb-4 text-center">
                                    <h5>Register</h5>
                                </div>
                                <form action="<?= BASEURL; ?>/register/prosesRegister" method="post">
                                    <div class="form-group mb-3">
                                        <input type="text" class="form-control" placeholder="nama" name="username" required>
                                    </div>
                                    <div class="form-group mb-3">
                                        <input type="text" class="form-control" placeholder="email" name="email" required>
                                    </div>
                                    <div class="form-group mb-3">
                                        <input type="password" class="form-control" placeholder="password" name="password" required>
                                    </div>
                                    <div class="text-center">
                                        <button class="btn bg-warning mb-3" style="width: 100%;">Register</button>
                                    </div>
                                    <p>Already have an account? <a href="<?= BASEURL; ?>/login" class="text-warning shadow-md">Login</a></p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
