<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman <?= $data['judul']; ?></title>
    <link rel="stylesheet" href="<?= BASEURL; ?>/css/bootstrap.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="<?= BASEURL; ?>/home">galerry kanvas</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
                <div class="navbar-nav"> <a class="nav-item nav-link active" href="<?= BASEURL; ?>">Beranda</a>
                    <?php if (isset($_SESSION['login'])) : ?>
                        <button type="login" class="btn btn-warning" id="login-button"><a href="<?= BASEURL; ?>/login/logout" class="text-white text-decoration-none">Logout</a></button>
                    <?php else : ?>
                        <button type="logout" class="btn btn-warning" id="logout-button"><a href="<?= BASEURL; ?>/login" class="text-white text-decoration-none">Login</a></button>
                    <?php endif; ?>

                </div>
        </div>
    </nav>