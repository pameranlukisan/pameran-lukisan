<?php

class Register extends Controller
{
    private $table = 'pengguna';
    private $db;


    public function __construct()
    {
        $this->db = new Database;
    }
    public function index()
    {
        if (isset($_SESSION['register'])) {
            header('Location: ' . BASEURL . '/home');
        }
        $data['judul'] = "Register";
        $this->view('register/index', $data);
    }

    public function prosesRegister()
    {
        $username = $_POST['username'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $passwordHash = password_hash($password, PASSWORD_DEFAULT);
        $query = "INSERT INTO pengguna (username, email, password, role)  VALUES (:username, :email, :password, 0)";
        $this->db->query($query);
        $this->db->bind('username', $username);
        $this->db->bind('email', $email);
        $this->db->bind('password', $passwordHash);
        $this->db->execute();
        
        header('Location: ' . BASEURL . '/login');
    }

    public function logout()
    {
        session_destroy();
        header('Location: ' . BASEURL . '/login');
        exit;
    }
}
