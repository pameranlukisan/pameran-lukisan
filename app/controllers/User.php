<?php
class User extends Controller
{

    public function __construct()
    {
        if (!isset($_SESSION['login'])) {
            header('location: ' . BASEURL . '/login');
        }
    }
    public function index()
    {
        // echo "Home/index";
        $data["judul"] = "User";
        $data["nama"] = $this->model("User_model")->getUser();
        $this->view("templates/header", $data);
        $this->view("user/index", $data);
        $this->view("templates/footer");
    }
}
