-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 13, 2023 at 06:04 AM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pameran_lukisan`
--

-- --------------------------------------------------------

--
-- Table structure for table `lukisan`
--

CREATE TABLE `lukisan` (
  `id_lukisan` int NOT NULL,
  `gambar_lukisan` varchar(255) NOT NULL,
  `nama_lukisan` varchar(50) NOT NULL,
  `ket_lukisan` varchar(100) NOT NULL,
  `id_pengguna` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `lukisan`
--

INSERT INTO `lukisan` (`id_lukisan`, `gambar_lukisan`, `nama_lukisan`, `ket_lukisan`, `id_pengguna`) VALUES
(1, '', 'Monalisa', 'Lokasi: Museum Louvre di Paris, Prancis\r\nArtis: Leonardo da Vinci\r\nDibuat: 1503 – 1519', 1),
(2, '', 'The Starry Night', 'Lokasi : Museum Seni, New York City, LA\r\nArtis : Vincent Van Gogh\r\nTahun : 1889', 2),
(3, '', 'The Persistence of Memory', 'Lokasi: Museum Seni Modern, New York\r\nArtis : Salvador Dali\r\nTahun : 1931', 3),
(4, '', 'Girl With A Pearl Earring', 'Lokasi : Mauritius, Belanda\r\nArtis : Johannes Vermeer\r\nTahun : 1665', 4),
(5, '', 'The Scream', 'Lokasi : Galeri Nasional Oslo, Norwegia\r\nArtis : Edvard Munch\r\nTahun : 1893', 5);

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id_pengguna` int NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `role` tinyint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id_pengguna`, `username`, `email`, `password`, `role`) VALUES
(7, 'admin', 'admin@gmail.com', '$2y$10$gb9Cz2CQaaWB7mhQo65ihusis0D1odn76wUwHECBGwYPgsNKupmnm', 1),
(8, 'Watanabe', 'watana@gmail.com', '$2y$10$ucNMwH7FBcZpkN48qQPsk.oX2gx0MJIwAd7AY0Dpk.dyco69c5mz6', 0),
(10, 'vira', 'vira@gmail.com', '$2y$10$VE1rdquTFJEz0v3ygs65Z.hERidaQsFSCvAJS7UkrVBPxQzB4JmHW', 0);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id_lukisan` int NOT NULL,
  `id_pengguna` int NOT NULL,
  `review` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id_lukisan`, `id_pengguna`, `review`) VALUES
(1, 1, 'menurut saya lukisan Mona Lisa oleh Leonardo da Vinci adalah lukisan ikonik dengan senyuman misterius dan teknik sfumato yang luar biasa. Lukisan ini memunculkan inspirasi yang luas dan telah memikat penonton selama berabad-abad.'),
(2, 2, 'Lukisan \"The Starry Night\" adalah gambaran indah bagaimana malam yang penuh bintang dengan desa kecil di bawahnya. Meskipun saya awam dalam seni, saya pribadi dapat merasakan daya tarik emosional dan keindahannya.'),
(3, 3, 'menurut saya lukisan The Persistence of Memory adalah lukisan surrealisme yang menggambarkan jam-jam melunak di tengah gurun. Lukisan ini mencerminkan ketertarikan Dalí pada ilmu pengetahuan dan teori relativitas.'),
(4, 4, 'Girl with a Pearl Earring adalah karya indah dan misterius yang menghadirkan seorang wanita dengan mata yang menawan dan mutiara di telinga. Lukisan ini bagi saya memancarkan keanggunan dan misteri yang tak terlupakan.'),
(5, 5, 'Lukisan The Scream adalah karya seni yang menurut saya ikonik yang menggambarkan ketakutan dan kecemasan dengan latar belakang yang aneh. Lukisan ini mencerminkan perasaan manusia pada masanya dan tetap relevan hingga sekarang.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lukisan`
--
ALTER TABLE `lukisan`
  ADD PRIMARY KEY (`id_lukisan`),
  ADD KEY `id_pengguna` (`id_pengguna`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id_pengguna`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id_lukisan`),
  ADD KEY `review_ibfk_5` (`id_pengguna`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lukisan`
--
ALTER TABLE `lukisan`
  MODIFY `id_lukisan` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id_pengguna` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`id_lukisan`) REFERENCES `lukisan` (`id_lukisan`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
