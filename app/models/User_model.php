<?php
class User_model
{
    private $table = 'pengguna';
    private $db;

    public function getUser()
    {
        return $this->table;
    }
    public function __construct()
    {
        $this->db = new Database;
    }
    // public function register($data)
    // {
    //     $query = "INSERT INTO users (username, password) VALUES (:username, :password)";
    //     $passwordHash = password_hash($data['password'], PASSWORD_DEFAULT);
    //     $this->db->query($query);
    //     $this->db->bind('username', $data['username']);
    //     $this->db->bind('password', $passwordHash);
    //     $this->db->execute();

    //     return 1;
    // }

    public function login($data)
    {
        $query = "SELECT * FROM pengguna WHERE email = :email";
        $this->db->query($query);
        $this->db->bind('email', $data['email']);
        $user = $this->db->single();
    
        if ($user) {
            $passwordPost = $data['password'];
            $passwordDb = $user['password'];
            if (password_verify($passwordPost, $passwordDb)) {
                // return 1;
                return $user;
            } else {
                echo '<script>alert("Email atau password yang Anda masukkan salah!");</script>';
                return 0;
            }
        } else {
            echo '<script>alert("Email atau password yang Anda masukkan salah!");</script>';
            return 0;
        }
    }
    
}
